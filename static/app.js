let socket = io.connect();


let context = {
    username : "",
    password : "toto",
    islogged : false,
    messages : {},
    message : "",
    currentFriend: "",
    notif : {},
    init() {
        socket.on('login',(s)=> {
            if (s == "OK") {
                this.islogged = true;
            }   
        })

        socket.on('message',(m) => {
            if (m.sender != this.currentFriend) {
                this.notif[m.sender] = true
            } 
            this.messages[m.sender].push(m)
        })
        socket.on('participants',(particpants) => {
            particpants.forEach(p => {
                this.messages[p] = []
                this.notif[p] = false
            });
        })
        socket.on('participantJoined',(p) => { 
            this.messages[p] = []
            this.notif[p] = false
        })
        socket.on('participantLeft',(p) => {
            if (p==this.currentFriend) {
                this.currentFriend = ""
            }
            delete this.messages[p]
            delete this.notif[p]
        })
    },

    loging() {
        payload =  {
            username : this.username,
            password : this.password
        }
        socket.emit("login", payload);
    },

    sendMessage() {
        if (this.message == "") return
        let message = {
            sender : this.username,
            text : this.message,
            date : "11/01/1968",
            receiver : this.currentFriend 
        }
        this.messages[this.currentFriend].push(message)
        socket.emit("message",message)
        this.message = ""
    },
    selectFriend(friend) {
        this.notif[friend]=false
        this.currentFriend = friend;
    }

}

document.addEventListener('alpine:initialized', () => {
    //
})
