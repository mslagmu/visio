


var app;
var con;


function getSessionID() {
    return localStorage["SLSession"]
}


function deconnexion() {
    localStorage["SLSession"] = ""
    conn.close()
}

function init() {
    app = new Vue({
        el: '#app',
        data: {
            messages: messages,
            currentFriend: "robot",
            sentmessage: "",
            newMessage: {},
            identification: "ok",
            password: "",
            initpurge: false
        },
        methods: {
            selectFriend(name) {
                this.currentFriend = name;
                delete this.newMessage[name]
                document.title = "SLChat";
                this.initpurge = false
            },
            sendPassword() {
                conn.send("PASSWORD§" + this.password);
                this.password = "";
            },
            sendMessage() {
                var msgst = {
                    Date: "Now",
                    Sender: "Moi",
                    Text: this.sentmessage
                }
                //this.messages[this.currentFriend].push(msgst)
                conn.send(this.currentFriend + "§" + this.sentmessage);

                this.sentmessage = ""
            },
            vider() {
                conn.send("EMPTY§" + this.currentFriend);
            },

            purge() {
                conn.send("DELETE§" + this.currentFriend);
                this.currentFriend = ""
                this.initpurge = false
            },

            keydown(e) {
                if (e.key == "Enter") {
                    e.preventDefault()
                    this.sendMessage()
                }
            },

            ping() {
                conn.send(this.currentFriend + "§ping");
            },
            displayFriend(name) {
                var s = name
                if (this.newMessage[name]) {
                    s = name + "*"
                }
                return s
            }

        }
    })




    function onmessage(evt) {
        var message = JSON.parse(evt.data);

        var action = message.Action

        if (action == "message") {
            var msg = message.Content;
            var name = message.Name

            if (msg.Sender != "Moi") notifyMe(name, msg.Text);

            if (app.messages.hasOwnProperty(name) == false) {
                Vue.set(app.messages, name, [])
            }
            app.messages[message.Name].push(msg)
            if (app.currentFriend != name) {
                Vue.set(app.newMessage, name, 1)

            }
            document.title = "*SLChat";

        }
        if (action == "list") {
            app.messages = message.Content;
        }

        if (action == "passwordok") {
            app.identification = 'ok'
            localStorage["SLSession"] = message.Content
        }

        if (action == "sessionok") {
            if (message.Content) {
                app.identification = 'ok'
            } else {
                app.identification = 'ko'
            }


        }


    };


    var wsurl = (document.location.protocol == "https:" ? "wss://" : "ws://") + document.location.host + "/ws"

    conn = new WebSocket(wsurl);
    conn.onmessage = onmessage
    conn.onopen = function () {
        console.log("Connexion ok")
        conn.send("SESSIONID§" + getSessionID());
    }

    function reconnection() {
        if (conn.readyState != 1) {
            conn.close()
            console.log("tentative de reconnexion")
            try {
                conn = new WebSocket(wsurl);
                conn.onmessage = onmessage
                conn.onopen = function () {
                    console.log("Connexion ok")
                    conn.send("SESSIONID§" + getSessionID());
                }
            } catch (e) {
                console.log("Reconnexion refusée")
            }

        }
    }

    setInterval(reconnection, 1000)


}

function notifyMe(name, message) {
    var options = {
        body: message,
        tag: "SL",
        icon: "/static/notif.png"
    }
    // Voyons si le navigateur supporte les notifications
    if (!("Notification" in window)) {
        alert("Ce navigateur ne supporte pas les notifications desktop");
    }

    // Voyons si l'utilisateur est OK pour recevoir des notifications
    else if (Notification.permission === "granted") {
        // Si c'est ok, créons une notification
        var notification = new Notification(name, options);
    }

    // Sinon, nous avons besoin de la permission de l'utilisateur
    // Note : Chrome n'implémente pas la propriété statique permission
    // Donc, nous devons vérifier s'il n'y a pas 'denied' à la place de 'default'
    else if (Notification.permission !== 'denied') {
        Notification.requestPermission(function (permission) {

            // Quelque soit la réponse de l'utilisateur, nous nous assurons de stocker cette information
            if (!('permission' in Notification)) {
                Notification.permission = permission;
            }

            // Si l'utilisateur est OK, on crée une notification
            if (permission === "granted") {
                var notification = new Notification(name, options);
            }
        });
    }

    // Comme ça, si l'utlisateur a refusé toute notification, et que vous respectez ce choix,
    // il n'y a pas besoin de l'ennuyer à nouveau.
}