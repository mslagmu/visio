var api
var participants =   {}
var message = document.querySelector('#message')
var namep = document.querySelector('#name')
function connectToRoom() {
    domain = 'meet.jit.si';
    let options = {
        roomName:"dhyenhljsunen",
        width: 1400,
        height: 700,
        parentNode: document.querySelector('#meet'),
        configOverwrite: { prejoinPageEnabled: false,
          toolbarButtons: [
                'camera',
                'chat',
                'closedcaptions',
                'desktop',
                'download',
                'embedmeeting',
                'etherpad',
                'feedback',
                'filmstrip',
                'fullscreen',
                //'hangup',
                'help',
                'invite',
                'livestreaming',
                'microphone',
                'mute-everyone',
                'mute-video-everyone',
                'participants-pane',
                'profile',
                'raisehand',
                //'recording',
                //'security',
                'select-background',
                'settings',
                'shareaudio',
                'sharedvideo',
                'shortcuts',
                'stats',
                'tileview',
                'toggle-camera',
                'videoquality',
                //'__end'
    ] },
        userInfo: {
            email: 'email@jitsiexamplemail.com',
            displayName: namep.value
        }
        
    }
    api = new JitsiMeetExternalAPI(domain, options)
    api.addListener("participantJoined", (p) => {
        participants[p.id] = p.displayName
        list()
    });
    api.addListener("participantLeft", (p) => {
        delete participants[p.id] 
        list()
    });
}


function list() {
    let m = ""
    for (const key in participants) {
        m = m +"<p>" + participants[key] + "</p>"
    }
    message.innerHTML=m
}

function disconnect() {
    api.dispose()
}