
import socketio

import uvicorn

sio = socketio.AsyncServer(async_mode='asgi')

nts = {}
stn = {}


@sio.event
async def connect(sid, environ, auth):
    pass


@sio.event
async def login(sid,payload):
    await sio.emit("login","OK",room=sid)
    await sio.emit("participants",[k for k in nts.keys()],room=sid)
    username = payload["username"]
    stn[sid] = username
    nts[username] = sid
    await sio.emit("participantJoined",username,skip_sid=sid)


@sio.event
async def message(sid,m):
    rsid = nts[m["receiver"]]
    await sio.emit("message",m,room=rsid)


@sio.event
async def disconnect(sid):
    name=stn[sid]
    await sio.emit("participantLeft",name,skip_sid=sid)
    del stn[sid]
    del nts[name]

static_files = {
    '/': 'static/index.html',
    '/static/': './static',
}


app = socketio.ASGIApp(sio,static_files=static_files)

if __name__=="__main__":
    uvicorn.run("main:app",reload=True)